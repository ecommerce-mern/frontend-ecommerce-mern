import React, { useEffect, useState } from 'react';
import { CardElement, useStripe, useElements } from '@stripe/react-stripe-js';
import { CheckOutlined } from '@ant-design/icons';

import { useHistory } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { createPaymentIntent } from '../functions/stripe';
import { createOrder, emptyUserCart } from '../functions/user';

const StripeCheckout = () => {
    const dispatch = useDispatch();
    const { user, coupon } = useSelector((state) => ({ ...state }));
    const history = useHistory();

    const [succeeded, setSucceeded] = useState(false);
    const [error, setError] = useState(null);
    const [processing, setProcessing] = useState('');
    const [disabled, setDisabled] = useState(true);
    const [clientSecret, setClientSecret] = useState(false);

    const stripe = useStripe();
    const elements = useElements();

    const cartStyle = {
        style: {
          base: {
            color: "#32325d",
            fontFamily: "Arial, sans-serif",
            fontSmoothing: "antialiased",
            fontSize: "16px",
            "::placeholder": {
              color: "#32325d",
            },
          },
          invalid: {
            color: "#fa755a",
            iconColor: "#fa755a",
          },
        },
      };

    useEffect(() => {
        createPaymentIntent(user.token, coupon)
            .then((res) => {
                setClientSecret(res.data.clientSecret);
            })
    }, []);

    const handleSubmit = async (e) => {
        e.preventDefault();
        setProcessing(true);
        const payload = await stripe.confirmCardPayment(clientSecret, {
            payment_method: {
                card: elements.getElement(CardElement),
                billing_details: {
                    name: e.target.name.value
                }
            }
        });

        if (payload.error) {
            setError(`Payment failed: ${payload.error.message}`);
            setProcessing(false);
        } else {
            createOrder(payload, user.token)
                .then((res) => {
                    if (res.data.ok) {
                        if (typeof window !== 'undefined') {
                            localStorage.removeItem('cart');
                            dispatch({
                                type: 'ADD_TO_CART',
                                payload: []
                            });
                            dispatch({
                                type: 'COUPON_APPLIED',
                                payload: false
                            })
                            emptyUserCart(user.token);
                        }
                    }
                })
            setError(null);
            setProcessing(false);
            setSucceeded(true);
            setTimeout(() => {
                history.push('/user/history');
            }, 1000);
        }
    }

    const handleChange = async (e) => {
        setDisabled(e.empty);
        setError(e.error ? e.error.message : '');
    }

    return (
        <>
            <form 
                id='payment-form'
                className='stripe-form'
                onSubmit={handleSubmit}
            >
                <CardElement id='card-element' options={cartStyle} onChange={handleChange} />
                {succeeded 
                ? <button className='stripe-button mt-5 mb-3'>
                    <CheckOutlined />
                </button>
                : <button className='stripe-button mt-5 mb-3' disabled={processing || disabled || succeeded}>
                    <span id='button-text'>
                        {processing ? <div className='spinner' id='spinner'></div> : 'Pay'}
                    </span>
                </button> 
                }
                { error && <div className='card-error' role='alert'>{error}</div> }
            </form>
        </>
    );

};

export default StripeCheckout;