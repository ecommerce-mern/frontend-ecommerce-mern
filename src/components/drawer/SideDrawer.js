import React from 'react';
import { Drawer, Button } from 'antd';
import { Link } from 'react-router-dom';

import { useSelector, useDispatch } from 'react-redux';
import laptop from '../../images/laptop.png';

const SideDrawer = () => {
    const dispatch = useDispatch();
    const { drawer, cart } = useSelector((state) => ({ ...state }));

    const imageStyle = {
        width: '100%',
        height: '50px',
        objectFit: 'cover'
    };

    return (
        <Drawer
            className='text-center'
            title={'Cart'}
            onClose={() => {
                dispatch({ 
                    type: 'SET_VISIBLE',
                    payload: false,
                });
            }} 
            visible={drawer}
        >
            {cart.map((p) => (
                <div key={p._id} className='row'>
                    <div className='col'>
                        {p.images[0] 
                            ? (
                                <>
                                    <img src={p.images[0].url} style={imageStyle} />
                                    <p className='text-center bg-primary text-light'>
                                        {p.title} {`(${p.count})`}
                                    </p>
                                </>) 
                            : (
                                <>
                                    <img src={laptop} style={imageStyle} />
                                    <p className='text-center bg-primary text-light'>
                                        {p.title} x {p.count}
                                    </p>
                                </>
                            )
                        }
                    </div>
                </div>
            ))}
            <Link to='/cart'>
                <button 
                    className='btn btn-primary btn-raised btn-block text-center' 
                    onClick={
                        () => 
                            (
                                dispatch({ 
                                    type: 'SET_VISIBLE',
                                    payload: false,
                                })
                    )}
                >
                    Go to cart
                </button>
            </Link>
        </Drawer>
    );

};

export default SideDrawer;