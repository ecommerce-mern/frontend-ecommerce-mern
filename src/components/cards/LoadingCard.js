import React from 'react';
import { Card, Skeleton } from 'antd';

const LoadingCard = ({ count }) => {

    const cards = () => {
        let totalCards = [];

        for (let c = 0; c < count; c++) {
            totalCards.push(
                <Card className='col-md-4' key={c}>
                    <Skeleton active></Skeleton>
                </Card>
            );
        };
        return totalCards;
    }

    return(
        <div className='row pb-5'>
            {cards()}
        </div>
    );
};

export default LoadingCard;