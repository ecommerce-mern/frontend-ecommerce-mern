import React, { useState } from 'react';
import { Card, Tooltip } from 'antd';
import { EyeOutlined, ShoppingCartOutlined } from '@ant-design/icons';
import { Link } from 'react-router-dom';
import StarRatings from 'react-star-ratings';

import _ from 'lodash';
import { useSelector, useDispatch } from 'react-redux';
import laptop from '../../images/laptop.png';
import { showAverage } from '../../functions/rating';

const { Meta } = Card;

const ProductCard = ({ product }) => {
    const { images, title, description, slug } = product;
    const [tooltip, setTooltip] = useState('Click to add');

    const dispatch = useDispatch();
    const { user, cart } = useSelector((state) => ({ ...state }));

    const handleAddToCart = () => {
        let cart = [];
        if (typeof window !== 'undefined') {
            if (localStorage.getItem('cart')) {
                cart = JSON.parse(localStorage.getItem('cart'));
            };
            cart.push({ ...product, count: 1 });
            let unique = _.uniqBy(cart, '_id');
            localStorage.setItem('cart', JSON.stringify(unique));
            setTooltip('Added');

            dispatch({ 
                type: 'ADD_TO_CART',
                payload: unique,
            });
            dispatch({ 
                type: 'SET_VISIBLE',
                payload: true,
            });
        };
    };
    
    return (
        <>
            { product && product.ratings && product.ratings.length > 0 
            ? showAverage(product) 
            : <div className='text-center pt-1 pb-3'>
                    <StarRatings 
                        rating={0}
                        starDimension='20px'
                        starSpacing='2px'
                        editing={false}
                    />
            </div>
            }
            <Card 
                cover={
                    <img src={images && images.length ? images[0].url : laptop} style={{ height: '150px', objectFit:'cover' }} className='p-1' />
                }
                actions={[
                    <Link to={`/product/${slug}`}> 
                        <EyeOutlined className='text-info' /> 
                        <br />
                        View product
                    </Link>, 
                    <Tooltip title={tooltip} >
                        <a onClick={handleAddToCart} disabled={product.quantity < 1}>
                            <ShoppingCartOutlined className='text-success'/>
                            <br />
                            {product.quantity < 1 ? 'Out of stock' : 'Add to Cart'}
                        </a>
                    </Tooltip>
                ]}
            >
                <Meta title={title} description={`${description && description.substring(0, 40)}...`} />
            </Card>
        </>
    );
};

export default ProductCard;