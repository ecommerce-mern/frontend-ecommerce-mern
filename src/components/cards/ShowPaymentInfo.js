import React from 'react';

const ShowPaymentInfo = ({ order, showStatus = true }) => {

    return (
        <>
        <div>
            Order ID: {order.paymentIntent.id}
        </div>
        <table className='table table-sm'>
            <tbody>
                <tr>
                    <td>Total: {(order.paymentIntent.amount / 100).toLocaleString('en-US', {
                        style: 'currency',
                        currency: 'USD'
                    })}
                    </td>
                    <td>Currency: {order.paymentIntent.currency.toUpperCase()}</td>
                    <td>Payment: {order.paymentIntent.status.toUpperCase()}</td>
                    <td>Ordered on: {new Date(order.paymentIntent.created * 1000).toLocaleString()}</td>
                </tr>
            </tbody>
        </table>
        {showStatus && <span className='badge bg-primary text-white p-3 mb-3'>Status: {order.orderStatus}</span>}
        </>
    );
};

export default ShowPaymentInfo;