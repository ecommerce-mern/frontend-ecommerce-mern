import React, { useState } from 'react';
import { Card, Tabs, Tooltip } from 'antd';
import { HeartOutlined, ShoppingCartOutlined } from '@ant-design/icons';
import ProductListItems from './ProductListItems';
import {Carousel} from 'react-responsive-carousel';
import StarRatings from 'react-star-ratings';
import RatingModal from '../modal/RatingModal';

import _ from 'lodash';
import { toast } from 'react-toastify';
import { useSelector, useDispatch } from 'react-redux';
import "react-responsive-carousel/lib/styles/carousel.min.css"; 
import laptop from '../../images/laptop.png';
import { showAverage } from '../../functions/rating';
import { addToWishlist } from '../../functions/user';

const { TabPane } = Tabs;

const SingleProduct = ({ product, onStarClick, star }) => {
    const { title, images, description, _id } = product;
    const [tooltip, setTooltip] = useState('Click to add');
    
    const dispatch = useDispatch();
    const { user, cart } = useSelector((state) => ({ ...state }));

    const handleAddToCart = () => {
        let cart = [];
        if (typeof window !== 'undefined') {
            if (localStorage.getItem('cart')) {
                cart = JSON.parse(localStorage.getItem('cart'));
            };
            // for (let productNum = 0; productNum < cart.length; productNum++) {
            //     if (p._id === product._id) {
                    
            //     }
            // }
            cart.push({ ...product, count: 1 });
            let unique = _.uniqBy(cart, '_id');
            localStorage.setItem('cart', JSON.stringify(unique));
            setTooltip('Added');

            dispatch({ 
                type: 'ADD_TO_CART',
                payload: unique,
            });
            dispatch({ 
                type: 'SET_VISIBLE',
                payload: true,
            });
        };
    };

    const handleAddToWishlist = (e) => {
        addToWishlist(product._id, user.token)
            .then((res) =>{
                toast.success('Added to wishlist');
        });
    };

    return (
        <>
            <div className="col-md-7">
                {images && images.length 
                ? 
                (
                    <Carousel showArrows={true} autoPlay infiniteLoop>
                        {images && images.map((i) => <img src={i.url} key={i.public_id} />)}
                    </Carousel>
                ) 
                : 
                (
                    <Card cover={<img src={laptop} className="mb-3 card-image" />}></Card>
                )}
                <Tabs type='card'>
                    <TabPane tab='Description' key={1}>
                        {description && description}
                    </TabPane>
                    <TabPane tab='More' key={2}>
                        Call us on xxxx-xxxx to learn more about this product, or view the manufacturer's website for more information.
                    </TabPane>
                </Tabs>
            </div>
            <div className='col-md-5'>
                <h1 className='bg-info p-3'>{title}</h1>
                { product && product.ratings && product.ratings.length > 0 
                ? showAverage(product) 
                : <div className='text-center pt-1 pb-3'>
                    <StarRatings 
                        rating={0}
                        starDimension='20px'
                        starSpacing='2px'
                        editing={false}
                    />
                </div>
                }
                <Card
                    actions={[
                        <Tooltip title={tooltip} >
                            <a onClick={handleAddToCart}>
                                <ShoppingCartOutlined className='text-success'/>
                                <br />
                                Add to Cart
                            </a>
                        </Tooltip>,
                        <a onClick={handleAddToWishlist} hidden={user && user.token ? false : true}>
                            <HeartOutlined className='text-info' />
                            <br />
                            Add to Wishlist
                        </a>,
                        <RatingModal>
                            <StarRatings 
                                name={_id}
                                numberOfStars={5}
                                rating={star}
                                changeRating={onStarClick}
                                isSelectable={true}
                                starRatedColor='red'
                            />
                        </RatingModal>
                    ]}
                >
                    <ProductListItems product={product} />
                </Card>
            </div>
        </>
    );
};

export default SingleProduct;