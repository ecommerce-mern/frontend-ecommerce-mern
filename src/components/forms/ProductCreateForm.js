import React from 'react';
import { Select } from 'antd';

const { Option } = Select;

const ProductCreateForm = ({ handleSubmit, handleChange, values, setValues, handleCategoryChange, subOptions, showSub }) => {
    
    const { title, description, price, category, categories, subs, shipping, quantity, images, colors, brands, color, brand } = values;

    return (
        <form onSubmit={handleSubmit}>
        <div className='form-group'>
            <label>Title</label>
            <input type='text' title='title' className='form-control' value={title} onChange={handleChange} />
        </div>
        <div className='form-group'>
            <label>Description</label>
            <textarea rows={7} cols={50} title='description' className='form-control' value={description} onChange={handleChange} />
            {/* <input type='text' title='description' className='form-control' value={description} onChange={handleChange} /> */}
        </div>
        <div className='form-group'>
            <label>Price</label>
            <input type='number' title='price' className='form-control' value={price} onChange={handleChange} />
        </div>
        <div className='form-group'>
            <label>Shipping</label>
            <select 
                title='shipping'
                className='form-control'
                onChange={handleChange}
            >
                <option>Please select</option>
                <option value='No'>No</option>
                <option value='Yes'>Yes</option>
            </select>
        </div>
        <div className='form-group'>
            <label>Quantity</label>
            <input type='number' title='quantity' className='form-control' value={quantity} onChange={handleChange} />
        </div>
        <div className='form-group'>
            <label>Color</label>
            <select 
                title='color'
                className='form-control'
                onChange={handleChange}
            >
                <option>Please select</option>
                {colors.map((c) => <option key={c} value={c}>{c}</option>)}
            </select>
        </div>
        <div className='form-group'>
            <label>Brand</label>
            <select 
                title='brand'
                className='form-control'
                onChange={handleChange}
            >
                <option>Please select</option>
                {brands.map((b) => <option key={b} value={b}>{b}</option>)}
            </select>
        </div>
        <div className='form-group'>
            <label>Category</label>
            <select 
                title='category' 
                className='form-control'
                onChange={handleCategoryChange}
            >
                <option>Please select</option>
                {categories.length > 0 && categories.map((c) => (
                    <option key={c._id} value={c._id}>
                        {c.name}
                    </option>
                ))}
            </select>
        </div>
        {showSub && (
            <div>
                <label>Subcategories</label>
                <Select
                    mode='multiple'
                    style={{width: '100%'}}
                    placeholder='Please select'
                    value={subs}
                    title='subs'
                    onChange={(value) => setValues({...values, subs: value})}
                >
                    {subOptions && subOptions.map((s) => (
                        <Option key={s._id} value={s._id}>{s.name}</Option> 
                    ))}
                </Select>
            </div>
        )}
        <br />
        <button type='submit' className='btn btn-outline-info'>Add</button>
    </form>
    );
};

export default ProductCreateForm;