import React from 'react';
import { Select } from 'antd';

const { Option } = Select;

const ProductUpdateForm = ({ handleSubmit, handleChange, values, setValues, handleCategoryChange, subOptions, showSub, categories, arrayOfSubs, setArrayOfSubs, selectedCategory }) => {
    
    const { title, description, price, category, subs, shipping, quantity, images, colors, brands, color, brand } = values;

    return (
        <form onSubmit={handleSubmit}>
        <div className='form-group'>
            <label>Title</label>
            <input type='text' title='title' className='form-control' value={title} onChange={handleChange} />
        </div>
        <div className='form-group'>
            <label>Description</label>
            <textarea rows={7} cols={50} title='description' className='form-control' value={description} onChange={handleChange} />
            {/* <input type='text' title='description' className='form-control' value={description} onChange={handleChange} /> */}
        </div>
        <div className='form-group'>
            <label>Price</label>
            <input type='number' title='price' className='form-control' value={price} onChange={handleChange} />
        </div>
        <div className='form-group'>
            <label>Shipping</label>
            <select 
                title='shipping'
                className='form-control'
                onChange={handleChange}
                value={shipping}
            >
                <option value='No'>No</option>
                <option value='Yes'>Yes</option>
            </select>
        </div>
        <div className='form-group'>
            <label>Quantity</label>
            <input type='number' title='quantity' className='form-control' value={quantity} onChange={handleChange} />
        </div>
        <div className='form-group'>
            <label>Color</label>
            <select 
                title='color'
                className='form-control'
                onChange={handleChange}
                value={color}
            >
                <option>Please select</option>
                {colors.map((c) => <option key={c} value={c}>{c}</option>)}
            </select>
        </div>
        <div className='form-group'>
            <label>Brand</label>
            <select 
                title='brand'
                className='form-control'
                onChange={handleChange}
                value={brand}
            >
                <option>Please select</option>
                {brands.map((b) => <option key={b} value={b}>{b}</option>)}
            </select>
        </div>
        <div className='form-group'>
            <label>Category</label>
            <select 
                title='category' 
                className='form-control'
                onChange={handleCategoryChange}
                value={selectedCategory ? selectedCategory : category._id}
            >
                {categories.length > 0 && categories.map((c) => (
                    <option 
                        key={c._id} 
                        value={c._id} 
                        // selected={c._id === category._id ? true : false}
                    >
                        {c.name}
                    </option>
                ))}
            </select>
        </div>
        <div>
            <label>Subcategories</label>
            <Select
                mode='multiple'
                style={{width: '100%'}}
                placeholder='Please select'
                value={arrayOfSubs}
                title='subs'
                onChange={(value) => setArrayOfSubs(value)}
            >
                {subOptions && subOptions.map((s) => (
                    <Option key={s._id} value={s._id}>{s.name}</Option> 
                ))}
            </Select>
            </div>
        <br />
        <button type='submit' className='btn btn-outline-info'>Submit</button>
    </form>
    );
};

export default ProductUpdateForm;