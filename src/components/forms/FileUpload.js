import React from 'react';
import Resizer from 'react-image-file-resizer';
import axios from 'axios';
import { useSelector } from 'react-redux';
import { Avatar, Badge } from 'antd';

const FileUpload = ({ values, setValues, setLoadingImg }) => {

    const { user } = useSelector((state) => ({ ...state }));

    const fileUploadAndResize = (e) => {
        let files = e.target.files;
        let allUploadedFiles = values.images;
        if (files) {
            setLoadingImg(true);
            for (let file_index = 0; file_index < files.length; file_index++) {
                Resizer.imageFileResizer(files[file_index], 720, 720, 'JPEG', 100, 0, (uri) => {
                    axios.post(`${process.env.REACT_APP_API}/uploadimages`, { image: uri }, {
                        headers: {
                            authtoken: user ? user.token : ''
                        }
                    })
                        .then((res) => {
                            setLoadingImg(false);
                            allUploadedFiles.push(res.data);
                            setValues({ ...values, images: allUploadedFiles })
                        })
                        .catch((err) => {
                            setLoadingImg(false);
                            console.log(err);
                        })
                }, 'base64');
            };
        };
    };

    const handleImageRemove = (public_id) => {
        setLoadingImg(true);
        axios.post(`${process.env.REACT_APP_API}/removeimage`, { public_id }, {
            headers: {
                authtoken: user ? user.token : ''
            }
        })
        .then((res) => {
            const { images } = values;
            let filteredImages = images.filter((img) => {
                return img.public_id !== public_id
            });
            setValues({ ...values, images: filteredImages });
            setLoadingImg(false);
        })
        .catch((err) => {
            setLoadingImg(false);
            console.log(err);
        });
    };

    return (
        <>
        <div className='row'>
            {values.images && values.images.map((image) => (
                <Badge 
                    key={image.public_id}  
                    count='X' 
                    onClick={() => handleImageRemove(image.public_id)}
                    style={{ cursor: 'pointer' }}
                >
                    <Avatar
                        src={image.url} 
                        size={100} 
                        className='ml-3'
                        shape='square' 
                    />
                </Badge>
            ))}
        </div>
        <br />
        <div className='row'>
            <label className='btn btn-primary btn-raised'> Choose file(s)
            <input 
                type='file' 
                multiple
                hidden 
                accept='images/*' 
                onChange={fileUploadAndResize}
            />
            </label>
        </div>
        </>
    );
};

export default FileUpload;