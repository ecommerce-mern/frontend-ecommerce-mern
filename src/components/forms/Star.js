import React from 'react';
import StarRatings from 'react-star-ratings';

const Star = ({ starClick, numOfStars }) => {

    return (
        <>
        <StarRatings 
            changeRating={() => starClick(numOfStars)}
            numberOfStars={numOfStars}
            starDimension='25px'
            starSpacing='2px'
            starHoverColor='red'
            starEmptyColor='red'
        />
        <br />
        </>
    );
};

export default Star;