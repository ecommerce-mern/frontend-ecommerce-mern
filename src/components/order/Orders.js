import React from 'react';
import { CheckCircleOutlined, CloseCircleOutlined } from '@ant-design/icons';
import ShowPaymentInfo from '../cards/ShowPaymentInfo';


const Orders = ({ orders, handleStatusChange }) => {

    const showOrderInTable = (order) => (
        <table className='table table-bordered'>
            <thead className='thead-light'>
                <tr>
                    <th scope='col'>Title</th>
                    <th scope='col'>Price</th>
                    <th scope='col'>Brand</th>
                    <th scope='col'>Color</th>
                    <th scope='col'>Count</th>
                    <th scope='col'>Shipping</th>
                </tr>
            </thead>
            <tbody>
                {order.products.map((p, i) => (
                    <tr key={i}>
                        <td><b>{p.product.title}</b></td>
                        <td>{p.product.price}</td>
                        <td>{p.product.brand}</td>
                        <td>{p.product.color}</td>
                        <td>{p.count}</td>
                        <td>{p.product.shipping === 'Yes' ? <CheckCircleOutlined style={{ color: 'green' }} /> : <CloseCircleOutlined style={{ color: 'red' }} />}</td>
                    </tr>
                ))}
            </tbody>
        </table>
    );

    return (
        <>
            {orders.map((order) => (
                <div key={order._id} className='container btn btn-block bg-light mb-5'>
                        <ShowPaymentInfo order={order} showStatus={false} />
                        <div className='row'>
                            <table className='table table-sm'>
                                <tr className={order.orderStatus === 'Completed' ? 'table-success' : order.orderStatus === 'Not processed' ? 'table-danger' : 'table-warning'}>
                                    <td>
                                        <span className='pr-5'>Delivery status</span>
                                        <select className='p-1' onChange={e => handleStatusChange(order._id, e.target.value)} defaultValue={order.orderStatus} name='status'>
                                            <option value='Not Processed'>Not Processed</option>
                                            <option value='Processing'>Processing</option>
                                            <option value='Dispatched'>Dispatched</option>
                                            <option value='Cancelled'>Cancelled</option>
                                            <option value='Completed'>Completed</option>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    {showOrderInTable(order)}
                </div>
            ))}
        </>
    );
};

export default Orders;