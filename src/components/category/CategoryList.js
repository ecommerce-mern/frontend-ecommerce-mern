import React, { useEffect, useState } from 'react';
import {Link} from 'react-router-dom';

import { getCategories } from '../../functions/category';

const CategoryList = () => {
    const [loading, setLoading] = useState(false);
    const [categories, setCategories] = useState([]);
    
    useEffect(() => {
        setLoading(true);
        getCategories().then((res) => {
            setCategories(res.data);
            setLoading(false);
        });

    }, []);

    const showCategories = () => (
        categories.map((c) => (
            <div key={c._id} className='col btn btn-outlined-primary btn-lg btn-block btn-raised m-2'>
                <Link to={`/category/${c.slug}`}>
                    {c.name}
                </Link>
            </div>)
        ));

    return (
        <div className='container'>
            <div className='row'>
                {loading ? (<h4 className='text-center'>Loading...</h4>) : showCategories()}
            </div>
        </div>
    );
};

export default CategoryList;