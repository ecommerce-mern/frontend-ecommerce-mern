import React, { useEffect, useState } from 'react';
import Jumbotron from '../components/cards/Jumbotron';
import NewArrivals from '../components/home/NewArrivals';
import BestSellers from '../components/home/BestSellers';
import CategoryList from '../components/category/CategoryList';
import SubList from '../components/sub/SubList';

import { 
    getProducts,
} from '../functions/product';

const Home = () => {
    const [products, setProducts] = useState([]);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        loadAllProducts();
    }, []);

    const loadAllProducts = () => {
        setLoading(true);
        getProducts(
            'createdAt',
            'desc',
            3
        ).then(res => {
            setProducts(res.data);
            setLoading(false);
            })
    };

    return (
        <>
        <div className='jumbotron text-danger h1 font-weight-bold text-center'>
            <Jumbotron text={['Latest Products', 'New Arrivals', 'Best Sellers']} />
        </div>
        <h4 className='text-center mt-5 mb-5 p-3 display-4 jumbotron'>
            New Arrivals
        </h4>
        <NewArrivals />
        <h4 className='text-center mt-5 mb-5 p-3 display-4 jumbotron'>
            Best Sellers
        </h4>
        <BestSellers />
        <h4 className='text-center mt-5 mb-5 p-3 display-4 jumbotron'>
            Categories
        </h4>
        <CategoryList />
        <h4 className='text-center mt-5 mb-5 p-3 display-4 jumbotron'>
            Subcategories
        </h4>
        <SubList />
        <br />
        <br />
        </>
    )
}

export default Home;