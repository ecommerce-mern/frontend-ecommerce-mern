import React, { useEffect, useState} from 'react';
import ReactQuill from 'react-quill';

import 'react-quill/dist/quill.snow.css';
import { toast } from 'react-toastify';
import { useSelector, useDispatch } from 'react-redux';
import { getUserCart, emptyUserCart, saveUserAddress, applyCoupon } from '../functions/user';

const Checkout = ({ history }) => {
    const [products, setProducts] = useState([]);
    const [total, setTotal] = useState(0);
    const [coupon, setCoupon] = useState('');
    const [totalAfterDiscount, setTotalAfterDiscount] = useState(0);
    const [discountError, setDiscountError] = useState('');
    const [address, setAddress] = useState('');
    const [addressSaved, setAddressSaved] = useState(false);

    const dispatch = useDispatch();
    const { user } = useSelector((state) => ({ ...state }));

    useEffect(() => {
        getUserCart(user.token)
            .then((res) => {
                setProducts(res.data.products);
                setTotal(res.data.cartTotal);
        });
    }, []);

    const emptyCart = () => {
        if (typeof window !== 'undefined') {
            localStorage.removeItem('cart');
        }

        dispatch({
            type: 'ADD_TO_CART',
            payload: []
        })

        emptyUserCart(user.token)
            .then((res) => {
                setProducts([]);
                setTotal(0);
                setTotalAfterDiscount(0);
                setCoupon('');
            })
    };

    const saveAddressToDb = () => {
        saveUserAddress(user.token, address)
            .then((res) => {
                if (res.data.ok) {
                    setAddressSaved(true);
                    toast.success('Address saved');
                } else {
                    toast.error('Unable to save address');
                }
            })
    };

    const showAddress = () => (
        <>
        <ReactQuill theme='snow' value={address} onChange={setAddress} />
        <button 
            className='btn btn-primary mt-2' 
            onClick={saveAddressToDb}
        > Save
        </button>
        </>
    );

    const showProductSummary = () => (
        <>
            {products.map((p, i) => (
                <div key={i}>
                    <p>{p.product.title} ({p.color}) x {p.count} = {p.price * p.count} </p>
                </div>
            ))}
        </>
    );

    const applyCouponDiscount = () => {
        applyCoupon(user.token, coupon)
            .then((res) => {
                if (res.data.err) {
                    toast.error('Coupon deemed invalid');
                    dispatch({
                        type: 'COUPON_APPLIED',
                        payload: true
                    })
                }
                else if (res.data) {
                    toast.success(`Coupon successfully applied`);
                    setTotalAfterDiscount(res.data);
                    dispatch({
                        type: 'COUPON_APPLIED',
                        payload: false
                    })
                }
            })
    };

    const showApplyCoupon = () => (
        <>
            <input 
                onChange={(e) => setCoupon(e.target.value)} 
                type='text' 
                value={coupon}
                className='form-control' 
            />
            <button 
                onClick={applyCouponDiscount}
                className='btn btn-primary mt-2'
            > Apply
            </button>
        </>
    );

    return (
        <div className='row'>
            <div className='col-md-6'>
                <h4>Delivery Address</h4>
                <br />
                <br />
                {showAddress()}
                <hr />
                <h4>Got coupon?</h4>
                <br />
                {showApplyCoupon()}
            </div>
            <div className='col-md-6'>
                <h4>Order summary</h4>
                <hr />
                {showProductSummary()}
                <hr />
                <p>Cart total ${total}</p>
                {totalAfterDiscount > 0 && (
                    <p> Total after discount: <b>${totalAfterDiscount}</b> </p>
                )}
                <div className='row'>
                    <div className='col-md-6'>
                        <button className='btn btn-primary' disabled={!addressSaved || !products.length} onClick={() => history.push('/payment')}>Place order</button>
                    </div>
                    <div className='col-md-6'>
                        <button disabled={!products.length} onClick={emptyCart} className='btn btn-primary'>Empty cart</button>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Checkout;