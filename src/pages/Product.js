import React, { useEffect, useState } from 'react';
import SingleProduct from '../components/cards/SingleProduct';
import ProductCard from '../components/cards/ProductCard';

import { getProduct, productStar, getRelated } from '../functions/product';
import { useSelector } from 'react-redux';

const Product = ({ match }) => {
    const [product, setProduct] = useState({});
    const [star, setStar] = useState(0);
    const [related, setRelated] = useState([]);

    const { slug } = match.params;
    const { user } = useSelector((state) => ({ ...state }));

    useEffect(() => {
        loadSingleProduct(slug);
    }, [slug]);

    useEffect(() => {
        if (product.ratings && user) {
            let existingRatingObject = product.ratings.find(
                (rating) => rating.postedBy.toString() === user._id.toString()
            );
            existingRatingObject && setStar(existingRatingObject.star);
        }
    });

    const loadSingleProduct = (slug) => {
        getProduct(slug)
            .then((res) => {
                setProduct(res.data);
                getRelated(res.data._id)
                    .then((res) => {
                        setRelated(res.data);
                    })
            });
    };

    const onStarClick = (newRating, name) => {
        setStar(newRating);
        productStar(name, newRating, user.token)
            .then(res => {
                loadSingleProduct(slug);
            })
    };

    return (
        <div className='container-fluid'>
            <div className='row pt-4'>
                <SingleProduct 
                    product={product} 
                    onStarClick={onStarClick}
                    star={star}    
                />
            </div>
            <div className='row p-5'>
                <div className='col text-center pt-1 pb-1'>
                    <hr />
                    <h4>
                        Related Products
                    </h4>
                    <hr />
                </div>
            </div>
            <div className='row pb-5'>
                {related.length 
                ? related.map(
                    (r) => <div key={r._id} className='col-md-3'><ProductCard product={r} /></div>
                  )
                : <div className='text-center col'>No Products Found</div>
                }
            </div>
        </div>
    );
};

export default Product;