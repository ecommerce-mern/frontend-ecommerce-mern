import React, { useState, useEffect } from 'react';
import AdminNav from '../../../components/nav/AdminNav';
import { toast } from 'react-toastify';
import { useSelector } from 'react-redux';
import CategoryForm from '../../../components/forms/CategoryForm';

import { getCategories } from '../../../functions/category';
import { 
    getSub,
    updateSub
} from '../../../functions/sub';

const SubUpdate = ({ match, history }) => {

    const {user} = useSelector(state => ({...state}));
    const [name, setName] = useState('');
    const [loading, setLoading] = useState(false);
    const [parent, setParent] = useState('');
    const [categories, setCategories] = useState([]);


    useEffect(() => {
        loadCategories();
        loadSub();
    });

    const loadCategories = () => {
        getCategories().then((c) => setCategories(c.data));
    };

    const loadSub = () => {
        getSub(match.params.slug)
            .then((s) => {
                setName(s.data.name);
                setParent(s.data.parent);
            });
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        setLoading(true);
        updateSub(match.params.slug, {name, parent}, user.token)
            .then(res => {
                setLoading(false);
                setName('');
                toast.success(`"${name}" successfully updated`);
                history.push('/admin/sub');
            })
            .catch(e => {
                setLoading(false);
                if (e.response.status === 400) {
                    toast.error(e.response.data);   
                }
            })
    };

    return (
        <div className='container-fluid'>
            <div className='row'>
                <div className='col-md-2'>
                    <AdminNav />
                </div>
                <div className='col'>
                    {loading ? <h4>Loading</h4> : <h4>Update Subcategory</h4>}
                    <div className='form-group'>
                        <label>Parent Category</label>
                        <select 
                            name='category' 
                            className='form-control'
                            onChange={(e) => setParent(e.target.value)}
                        >
                            {categories.length > 0 && categories.map((c) => (
                                <option key={c._id} value={c._id} selected={c._id === parent} >
                                    {c.name}
                                </option>
                            ))}
                        </select>
                    </div>
                    <CategoryForm 
                        handleSubmit={handleSubmit} 
                        name={name}
                        setName={setName}
                    />
                </div>
            </div>
        </div>
    );
};

export default SubUpdate;