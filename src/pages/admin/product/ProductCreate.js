import React, { useState, useEffect } from 'react';
import AdminNav from '../../../components/nav/AdminNav';
import { toast } from 'react-toastify';
import { useSelector } from 'react-redux';
import ProductCreateForm from '../../../components/forms/ProductCreateForm';
import FileUpload from '../../../components/forms/FileUpload';
import { LoadingOutlined } from '@ant-design/icons';

import {
    getCategories,
    getCategorySubs
} from '../../../functions/category';

import {
    createProduct,
} from '../../../functions/product';

const initialState = {
    title: '',
    description: '',
    price: '',
    categories: [],
    category: '',
    subs: [],
    shipping: '',
    quantity: '',
    images: [],
    colors: ['Black', 'Brown', 'Silver', 'White', 'Blue'],
    brands: ['Apple', 'Microsoft', 'ASUS', 'Lenovo', 'Samsung'],
    color: '',
    brand: ''
};

const ProductCreate = () => {
    const [values, setValues] = useState(initialState);
    const [subOptions, setSuboptions] = useState([]);
    const [showSub, setShowSub] = useState(false);
    const [loadingImg, setLoadingImg] = useState(false);
    const { user } = useSelector((state) => ({...state}));

    useEffect(() => {
        loadCategories();
    }, []);

    const loadCategories = () => {
        getCategories().then((c) => setValues({...values, categories: c.data}));
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        createProduct(values, user.token)
            .then(res => {
                toast.success(`Product with name "${values.title}" successfully created`);
                setValues(initialState);
            })
            .catch(err => {
                toast.error(err.response.data.err);
            });
    };

    const handleChange = (e) => {
      setValues({...values, [e.target.title]: e.target.value});
    };

    const handleCategoryChange = (e) => {
        e.preventDefault();
        setValues({...values, subs: [], category: e.target.value});
        getCategorySubs(e.target.value)
            .then((res) => {
                setSuboptions(res.data);
            });
        setShowSub(true);
    };

    return (
        <div className='container-fluid'>
            <div className='row'>
                <div className='col-md-2'>
                    <AdminNav />
                </div>
                <div className='col-md-10'>
                    { loadingImg ? <LoadingOutlined className='text-danger h1' /> : <h4>Product create</h4>}
                    <hr />
                    <div className='p-3'>
                        <FileUpload 
                            values={values}
                            setValues={setValues}
                            setLoadingImg={setLoadingImg}
                        />
                    </div>
                    <ProductCreateForm 
                        handleSubmit={handleSubmit} 
                        handleChange={handleChange} 
                        values={values} 
                        setValues={setValues}
                        handleCategoryChange={handleCategoryChange}
                        subOptions={subOptions}
                        showSub={showSub}
                    />
                </div>
            </div>
        </div>
    );
};

export default ProductCreate;