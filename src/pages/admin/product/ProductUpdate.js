import React, { useState, useEffect } from 'react';
import AdminNav from '../../../components/nav/AdminNav';
import ProductUpdateForm from '../../../components/forms/ProductUpdateForm';
import FileUpload from '../../../components/forms/FileUpload';
import { LoadingOutlined } from '@ant-design/icons';

import { toast } from 'react-toastify';
import { useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import {
    getCategories,
    getCategorySubs,
    updateCategory
} from '../../../functions/category';

import {
    getProduct,
    updateProduct,
} from '../../../functions/product';

const initialState = {
    title: '',
    description: '',
    price: '',
    category: '',
    subs: [],
    shipping: '',
    quantity: '',
    images: [],
    colors: ['Black', 'Brown', 'Silver', 'White', 'Blue'],
    brands: ['Apple', 'Microsoft', 'ASUS', 'Lenovo', 'Samsung'],
    color: '',
    brand: ''
};

const ProductUpdate = ({ match, history }) => {
    const [values, setValues] = useState(initialState);
    const [subOptions, setSuboptions] = useState([]);
    const [arrayOfSubs, setArrayOfSubs] = useState([]);
    const [categories, setCategories] = useState([]);
    const [selectedCategory, setSelectedCategory] = useState('');
    const [loadingImg, setLoadingImg] = useState(false);

    const { user } = useSelector((state) => ({...state}));
    const { slug } = match.params;

    useEffect(() => {
        loadProduct();
        loadCategories();
    }, []);

    const loadProduct = () => {
        getProduct(slug)
            .then((p) => {
                setValues({ ...values, ...p.data });
                getCategorySubs(p.data.category._id)
                    .then((res) => {
                        setSuboptions(res.data);
                    });
                let arr = [];
                p.data.subs.map((s) => {
                    arr.push(s._id);
                });
                setArrayOfSubs((prev) => arr);
            });
    };

    const loadCategories = () => {
        getCategories().then((c) => setCategories(c.data));
    };


    const handleSubmit = (e) => {
        e.preventDefault();
        setLoadingImg(true);
        values.subs = arrayOfSubs;
        values.category = selectedCategory ? selectedCategory : values.category;

        updateProduct(slug, values, user.token)
            .then((res) => {
                setLoadingImg(false);
                toast.success(`${values.title} successfully updated`);
                history.push('/admin/products');
            })
            .catch((err) => {
                setLoadingImg(false);
                toast.error(err.response.data.err);
            });
    };

    const handleChange = (e) => {
        setValues({...values, [e.target.title]: e.target.value});
      };

    const handleCategoryChange = (e) => {
        //e.preventDefault();
        setValues({...values, subs: []});
        setSelectedCategory(e.target.value);
        getCategorySubs(e.target.value)
            .then((res) => {
                setSuboptions(res.data);
            });
        if (values.category._id === e.target.value) {
            loadProduct();
        };
        setArrayOfSubs([]);
    };

    return (
        <div className='container-fluid'>
            <div className='row'>
                <div className='col-md-2'>
                    <AdminNav />
                </div>
                <div className='col-md-10'>
                { loadingImg ? <LoadingOutlined className='text-danger h1' /> : <h4>Product update</h4>}
                    <hr />
                    <div className='p-3'>
                        <FileUpload 
                            values={values}
                            setValues={setValues}
                            setLoadingImg={setLoadingImg}
                    />
                    </div>
                    <ProductUpdateForm 
                        handleSubmit={handleSubmit}
                        handleChange={handleChange}
                        handleCategoryChange={handleCategoryChange}
                        values={values}
                        setValues={setValues}
                        categories={categories}
                        subOptions={subOptions}
                        arrayOfSubs={arrayOfSubs}
                        setArrayOfSubs={setArrayOfSubs}
                        selectedCategory={selectedCategory}
                    />
                </div>
            </div>
        </div>
    );
};

export default ProductUpdate;