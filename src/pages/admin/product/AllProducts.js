import React, { useState, useEffect } from 'react';
import AdminNav from '../../../components/nav/AdminNav';
import AdminProductCard from '../../../components/cards/AdminProductCard';

import { toast } from 'react-toastify';
import { useSelector } from 'react-redux';
import { 
    getProductsByCount,
    removeProduct
} from '../../../functions/product';

const AllProducts = () => {
    const [products, setProducts] = useState([]);
    const [loading, setLoading] = useState(false);
    const { user } = useSelector((state) => ({ ...state }));

    const loadAllProducts = () => {
        setLoading(true);
        getProductsByCount(100)
            .then((res) => {
                setProducts(res.data);
                setLoading(false);
            })
            .catch(error => {
                setLoading(false);
                console.log(error);
            });
    };

    const handleRemove = (slug) => {
        let answer = window.confirm('Delete?');
        if(answer) {
            removeProduct(slug, user.token)
                .then((res) => {
                    loadAllProducts();
                    toast.error(`${res.data.title} successfully deleted`);
                })
                .catch((err) => {
                    console.log(err);
                    if (err.response.status === 400) toast.error(err);
                });
        };
    };

    useEffect(() => {
        loadAllProducts();
    }, []);

    return (
        <div className='container-fluid'>
            <div className='row'>
                <div className='col-md-2'>
                    <AdminNav />
                </div>
                <div className='col'>
                    {loading ? (<h4>Loading...</h4>) : (<h4>All Products</h4>)}
                    <div className='row'>
                        {products.map(p => (
                            <div key={p._id} className='col-md-4 pb-3'>
                                <AdminProductCard 
                                    product={p} 
                                    handleRemove={handleRemove} 
                                />
                            </div>
                        ))}
                    </div>
                </div>
            </div>
        </div>
    );
};

export default AllProducts;