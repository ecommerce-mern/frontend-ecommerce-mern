import React, { useState, useEffect } from 'react';
import AdminNav from '../../../components/nav/AdminNav';
import { toast } from 'react-toastify';
import { useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import CategoryForm from '../../../components/forms/CategoryForm';

import {
    updateCategory,
    getCategory,
} from '../../../functions/category';

const CategoryUpdate = ({ history, match }) => {

    const {user} = useSelector(state => ({...state}));
    let [oldName, setOldName] = useState('');
    const [name, setName] = useState('');
    const [loading, setLoading] = useState(false);

    let { slug } = useParams();

    const loadCategory = () => {
        getCategory(match.params.slug)
            .then((c) => {
                setOldName(c.data.name);
                setName(c.data.name);
            });
    };

    useEffect(() => {
        loadCategory();
    })

    const handleSubmit = (e) => {
        e.preventDefault();
        setLoading(true);
        updateCategory(slug, {name}, user.token)
            .then(res => {
                setLoading(false);
                toast.success(`"${oldName}" updated to "${res.data.name}"`);
                history.push('/admin/category');
            })
            .catch(e => {
                setLoading(false);
                if (e.response.status === 400) {
                    toast.error(e.response.data);   
                }
            })
    };

    return (
        <div className='container-fluid'>
            <div className='row'>
                <div className='col-md-2'>
                    <AdminNav />
                </div>
                <div className='col'>
                    {loading ? <h4>Loading</h4> : <h4>Update category</h4>}
                    <CategoryForm 
                        handleSubmit={handleSubmit} 
                        name={name}
                        setName={setName}
                    />
                </div>
            </div>
        </div>
    );
};

export default CategoryUpdate;