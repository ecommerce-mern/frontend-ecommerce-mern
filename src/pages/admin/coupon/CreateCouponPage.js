import React, { useState, useEffect } from 'react';
import DatePicker from 'react-datepicker';
import { DeleteOutlined } from '@ant-design/icons';
import AdminNav from '../../../components/nav/AdminNav';

import { useSelector, useDispatch } from 'react-redux';
import { toast } from 'react-toastify';
import { getCoupons, removeCoupon, createCoupon } from '../../../functions/coupon';
import "react-datepicker/dist/react-datepicker.css";


const CreateCouponPage = () => {
    const [name, setName] = useState('');
    const [expiry, setExpiry] = useState(new Date());
    const [discount, setDiscount] = useState('');
    const [isPercentage, setIsPercentage] = useState(true);
    const [loading, setLoading] = useState('');
    const [coupons, setCoupons] = useState([]);

    const {user} = useSelector(state => ({...state}));

    useEffect(() => {
        loadCoupons();
    }, []);

    const loadCoupons = () => {
        getCoupons()
        .then((res) => {
            setCoupons(res.data);
        })
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        setLoading(true);
        const coupon = {name, expiry, discount, isPercentage};
        createCoupon(coupon, user.token)
            .then((res) => {
                toast.success(`Coupon "${name}" successfully created`);
                setLoading(false);
                setName('');
                setExpiry(new Date());
                setDiscount('');
                loadCoupons();
            })
            .catch((err) => {
                toast.error(`${err.message}`);
            })
    };

    const handleRemove = (couponId) => {
        if (window.confirm('Delete?')) {
            setLoading(true);
            removeCoupon(couponId, user.token)
                .then((res) => {
                    loadCoupons();
                    setLoading(false);
                })
                .catch((err) => {
                    toast.error(`${err.message}`);
                    setLoading(false);
            });
        };
    };

    return (
        <div className='container-fluid'>
            <div className='row'>
                <div className='col-md-2'>
                    <AdminNav />
                </div>
                <div className='col-md-10'>
                    <h4>Create Coupons</h4>
                    <form onSubmit={handleSubmit}>
                        <div className='form-group'>
                            <label className='text-muted'>Name</label>
                            <input 
                                type='text' 
                                className='form-control' 
                                onChange={(e) => setName(e.target.value)} 
                                value={name} 
                                autoFocus
                                required
                            />
                        </div>
                        <div className='form-group row'>
                            <div className='col-sm-1'>
                                <select 
                                    title='percentageornot'
                                    className='form-control pl-3 mt-4 border border-light'
                                    onChange={(e) => setIsPercentage(e.target.value)}
                                    value={isPercentage}
                                >
                                    <option value={true}>%</option>
                                    <option value={false}>$</option>
                                </select>
                            </div>
                            <div className='col'>
                                <label className='text-muted'>Discount</label>
                                <input 
                                    type='text' 
                                    className='form-control' 
                                    onChange={(e) => setDiscount(e.target.value)} 
                                    value={discount} 
                                    autoFocus
                                    required
                                />
                            </div>
                        </div>
                        <div className='form-group'>
                            <label className='text-muted'>Expiry Date</label>
                            <br />
                            <DatePicker 
                                className='form-control'
                                value={expiry} 
                                selected={expiry}
                                required 
                                onChange={(date) => setExpiry(date)}  
                            />
                        </div>
                        <button className='btn btn-outline-primary'>Save</button>
                    </form>
                    <br />
                    {loading ? <h4>Loading...</h4> : <h4>Coupons</h4>}
                    <table className='table table-bordered'>
                        <thead className='thead-light'>
                            <tr>
                                <th scope='col'>Name</th>
                                <th scope='col'>Expiry</th>
                                <th scope='col'>Discount</th>
                                <th scope='col'>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {coupons.map((c) => (
                                <tr key={c._id}>
                                    <td>{c.name}</td>
                                    <td>{new Date(c.expiry).toLocaleDateString()}</td>
                                    <td>{c.discount} {c.isPercentage ? '%' : '$'}</td>
                                    <td>
                                        <DeleteOutlined onClick={() => handleRemove(c._id)} className='text-danger pointer' />
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    );
};

export default CreateCouponPage;