import React, { useState, useEffect } from 'react';
import ProductCard from '../components/cards/ProductCard';
import Star from '../components/forms/Star';
import { Menu, Slider, Checkbox, Radio } from 'antd';
import { DollarOutlined, DownSquareOutlined, StarOutlined } from '@ant-design/icons';

import { fetchProductsByFilter, getProductsByCount } from '../functions/product';
import { useSelector, useDispatch } from 'react-redux';
import { getCategories } from '../functions/category';
import { getSubs } from '../functions/sub';

const { SubMenu, ItemGroup } = Menu;

const Shop = () => {
    const [products, setProducts] = useState([]);
    const [brands, setBrands] = useState(['Apple', 'Microsoft', 'ASUS', 'Lenovo', 'Samsung', 'HP']);
    const [brand, setBrand] = useState('');
    const [categories, setCategories] = useState([]);
    const [subs, setSubs] = useState([]);
    const [sub, setSub] = useState('');
    const [loading, setLoading] = useState(false);
    const [price, setPrice] = useState([0, 0]);
    const [ok, setOk] = useState(false);
    const [categoryIds, setCategoryIds] = useState([]);
    const [star, setStar] = useState('');
    const [colors, setColors] = useState(['Black', 'Brown', 'Silver', 'White', 'Blue']);
    const [color, setColor] = useState('');
    const [shipping, setShipping] = useState('');

    let dispatch = useDispatch();
    const { search } = useSelector((state) => ({ ...state }));
    const { text } = search;

    const fetchProducts = (arg) => {
        fetchProductsByFilter(arg)
            .then((res) => {
                setProducts(res.data);
            });
    };

    const loadAllProducts = () => {
        setLoading(true);
        getProductsByCount(12)
            .then((res) => {
                setProducts(res.data);
                setLoading(false);
            })
    };

    // default products
    useEffect(() => {
        loadAllProducts();
        getCategories()
            .then((res) => {
                setCategories(res.data);
            });
        getSubs()
            .then((res) => {
                setSubs(res.data);
            })
    }, []);

    // products when search input
    useEffect(() => {
        const delayed = setTimeout(() => {
            fetchProducts({query: text});
            if (!text) {
                loadAllProducts();
            }
        }, 300);
        return () => clearTimeout(delayed);
    }, [text]);

    // products based on price
    useEffect(() => {
        const delayed = setTimeout(() => {
            fetchProducts({price});
        }, 300);
        return () => clearTimeout(delayed);
    }, [ok]);
    const handleSlider = (value) => {
        dispatch({
            type: 'SEARCH_QUERY',
            payload: { text: '' }
        })
        setBrand('');
        setCategoryIds([]);
        setPrice(value);
        setStar('');
        setSub('');
        setColor('');
        setShipping('');
        setTimeout(() => {
            setOk(!ok);
        }, 300);
    };

    // products based on categories
    const handleCheck = (e) => {
        dispatch({
            type: 'SEARCH_QUERY',
            payload: { text: '' }
        });
        setBrand('');
        setPrice([0, 0]);
        setStar('');
        setSub('');
        setColor('');
        setShipping('');
        let inTheState = [...categoryIds];
        let justChecked = e.target.value;
        let foundInState = inTheState.indexOf(justChecked);
        if (foundInState === -1) {
            inTheState.push(justChecked);
        } else {
            inTheState.splice(foundInState, 1);
        };
        setCategoryIds(inTheState);
        fetchProducts({ category: inTheState });
    };
    const showCategories = () => categories.map((c) => (
        <div key={c._id}>
            <Checkbox 
                className='pb-2 pl-5' 
                value={c._id} 
                checked={categoryIds.includes(c._id) ? true : false}
                name='category' 
                onChange={handleCheck}
            >
                {c.name}
            </Checkbox>
            <br />
        </div>
    ));

    // products based on stars
    const handleStarClick = (num) => {
        dispatch({
            type: 'SEARCH_QUERY',
            payload: { text: '' }
        });
        setBrand('');
        setPrice([0, 0]);
        setCategoryIds([]);
        setStar(num);
        setSub('');
        setColor('');
        setShipping('');
        fetchProducts({ stars: num });
    };
    const showStars = () => (
        <div className='pr-4 pl-5 pb-2'>
            {[5,4,3,2,1].map((n) => (
                <Star key={n} starClick={handleStarClick} numOfStars={n} />
            ))}
        </div>
    );

    //products based on subs
    const showSubs = () => (
        subs.map((s) => (
            <div 
                key={s._id} 
                onClick={() => handleSub(s)} 
                className='p-1 m-1 badge badge-primary'
                style={{ cursor: 'pointer' }}    
            >
                {s.name}
            </div>
        ))
    );
    const handleSub = (s) => {
        setSub(s);
        dispatch({
            type: 'SEARCH_QUERY',
            payload: { text: '' }
        });
        setBrand('');
        setPrice([0, 0]);
        setCategoryIds([]);
        setStar('');
        setColor('');
        setShipping('');
        fetchProducts({ sub: s });
    };

    // products based on brands
    const showBrands = () => (
        brands.map((b) => (
            <Radio 
                value={b} 
                name={b} 
                key={b} 
                checked={b === brand} 
                onChange={handleBrand}
                className="col"
            >
                {b}
            </Radio>
        ))
    );
    const handleBrand = (e) => {
        setSub('');
        dispatch({
            type: 'SEARCH_QUERY',
            payload: { text: '' }
        });
        setPrice([0, 0]);
        setCategoryIds([]);
        setStar('');
        setColor('');
        setShipping('');
        setBrand(e.target.value);
        fetchProducts({ brand: e.target.value });
    };

    // products based on colors
    const showColors = () => (
        colors.map((c) => (
            <Radio 
                value={c} 
                name={c} 
                key={c} 
                checked={c === color} 
                onChange={handleColor}
                className="col"
            >
                {c}
            </Radio>
        ))
    );
    const handleColor = (e) => {
        setSub('');
        setBrand('');
        dispatch({
            type: 'SEARCH_QUERY',
            payload: { text: '' }
        });
        setPrice([0, 0]);
        setCategoryIds([]);
        setStar('');
        setShipping('');
        setColor(e.target.value);
        fetchProducts({ color: e.target.value });
    };

    // products based on shipping
    const showShipping = () => (
        <>
            <Checkbox 
                className='pl-4 pr-4 pb-2' 
                onChange={handleShippingChange}
                value='Yes'
                checked={shipping === 'Yes'}
            >
                Yes
            </Checkbox>
            <Checkbox 
                className='pl-4 pr-4 pb-2' 
                onChange={handleShippingChange}
                value='No'
                checked={shipping === 'No'}
            >
                No
            </Checkbox>
        </>
    );
    const handleShippingChange = (e) => {
        setSub('');
        setBrand('');
        dispatch({
            type: 'SEARCH_QUERY',
            payload: { text: '' }
        });
        setPrice([0, 0]);
        setCategoryIds([]);
        setStar('');
        setColor('');
        setShipping(e.target.value);
        fetchProducts({ shipping: e.target.value });
    };

    const showOptions = () => (
        subMenuItemDetails.map((item) => (
            <SubMenu key={item.key} title={<span className='h6'>< DownSquareOutlined />{item.title}</span>}>
                <div className={item.classes}>
                    {item.func}
                </div>
            </SubMenu>
        ))
    );

    const subMenuItemDetails = [
        {key: 'categories', title: 'Categories', func: showCategories(), classes: 'pt-2'}, 
        {key: 'subcategories', title: 'Subcategories', func: showSubs(), classes: 'pt-2 pl-5'}, 
        {key: 'brands', title: 'Brands', func: showBrands(), classes: 'pl-5 pr-4'}, 
        {key: 'color', title: 'Color', func: showColors(), classes: 'pl-5 pr-4'}, 
        {key: 'shipping', title: 'Shipping', func: showShipping(), classes: 'pl-5 pr-4'}
    ]

    return (
        <div className='container-fluid'>
            <div className='row'>
                <div className='col-md-3 pt-3'>
                    <h4>
                        Search/filter    
                    </h4> 
                    <hr />
                    <Menu defaultOpenKeys={['price', 'categories', 'subcategories', 'stars', 'brands', 'color', 'shipping']} mode='inline'>
                        <SubMenu key='price' title={<span className='h6'><DollarOutlined />Price</span>}>
                            <div>
                                <Slider 
                                    className='ml-4 mr-4' 
                                    tipFormatter={(v) => `$${v}`} 
                                    range 
                                    value={price}
                                    onChange={handleSlider}
                                    max='4999'
                                />
                            </div>
                        </SubMenu>
                        <SubMenu key='stars' title={<span className='h6'>< StarOutlined />Stars</span>}>
                            <div className='pt-2'>
                                {showStars()}
                            </div>
                        </SubMenu>
                        { showOptions() }
                    </Menu>
                </div>
                <div className='col-md-9 pt-2'>
                    { loading ? (
                        <h4 className='text-danger'>
                            Loading
                        </h4>
                    ) : (
                        <h4 className='text-danger'>
                            Products
                        </h4>
                    )}
                    {products.length < 1 && <p>No products found</p>}
                    <div className='row pb-5'>
                        {products.map((p) => (
                            <div key={p._id} className='col-md-4 pt-5'>
                                <ProductCard product={p} />
                            </div>
                        ))}
                    </div>
                </div>
            </div>
        </div>
    );

};

export default Shop;