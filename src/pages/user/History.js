import React, { useEffect, useState } from 'react';
import UserNav from '../../components/nav/UserNav';
import ShowPaymentInfo from '../../components/cards/ShowPaymentInfo';
import { CheckCircleOutlined, CloseCircleOutlined } from '@ant-design/icons';
import { PDFDownloadLink } from '@react-pdf/renderer';
import Invoice from '../../components/order/Invoice';

import { toast } from 'react-toastify';
import { useSelector, useDispatch } from 'react-redux';
import { getUserOrders } from '../../functions/user';

const History = () => {
    const [orders, setOrders] = useState([]);
    const { user } = useSelector((state) => ({ ...state }));

    useEffect(() => {
        loadUserOrders();
    }, []);

    const loadUserOrders = () => {
        getUserOrders(user.token)
            .then((res) => {
                setOrders(res.data);
            })
    };

    const showOrderInTable = (order) => (
        <table className='table table-bordered'>
            <thead className='thead-light'>
                <tr>
                    <th scope='col'>Title</th>
                    <th scope='col'>Price</th>
                    <th scope='col'>Brand</th>
                    <th scope='col'>Color</th>
                    <th scope='col'>Count</th>
                    <th scope='col'>Shipping</th>
                </tr>
            </thead>
            <tbody>
                {order.products.map((p, i) => (
                    <tr key={i}>
                        <td><b>{p.product.title}</b></td>
                        <td>{p.product.price}</td>
                        <td>{p.product.brand}</td>
                        <td>{p.product.color}</td>
                        <td>{p.count}</td>
                        <td>{p.product.shipping === 'Yes' ? <CheckCircleOutlined style={{ color: 'green' }} /> : <CloseCircleOutlined style={{ color: 'red' }} />}</td>
                    </tr>
                ))}
            </tbody>
        </table>
    );

    const showDownloadLink = (order) => (
        <PDFDownloadLink document={
            <Invoice order={order} />
        }
        fileName='invoice.pdf'
        className='btn btn-sm btn-outline-primary'
        >
            Download PDF
        </PDFDownloadLink>
    );

    const showEachOrder = () => (
        orders.reverse().map((order, i) => (
            <div key={i} className='card m-5 p-3'>
                <ShowPaymentInfo order={order} />
                {showOrderInTable(order)}
                <div className='row'>
                    <div className='col'>
                        {showDownloadLink(order)}
                    </div>
                </div>
            </div>
        ))
    );

    return (
        <div className='container-fluid'>
            <div className='row'>
                <div className='col-md-2'>
                    <UserNav />
                </div>
                <div className='col text-center pt-2'>
                    <h4>
                        {orders.length > 0 ? 'Your orders' : 'No purchase orders'}
                    </h4>
                    {showEachOrder()}
                </div>
            </div>
        </div>
    );
};

export default History;