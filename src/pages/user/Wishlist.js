import React, { useEffect, useState } from 'react';
import UserNav from '../../components/nav/UserNav';
import { Link } from 'react-router-dom';
import { DeleteFilled } from '@ant-design/icons';

import { useSelector, useDispatch } from 'react-redux';
import { getWishlist, removeWishlist } from '../../functions/user';

const Wishlist = () => {
    const [wishlist, setWishlist] = useState([]);
    const { user } = useSelector((state) => ({ ...state }));

    useEffect(() => {
        loadWishlist()
    }, []);

    const loadWishlist = () => {
        getWishlist(user.token)
            .then((res) => {
                setWishlist(res.data.wishlist);
        });
    };

    const handleRemove = (productId) => {
        removeWishlist(productId, user.token)
            .then((res) => {
                loadWishlist();
            })
    };

    return (
        <div className='container-fluid'>
            <div className='row'>
                <div className='col-md-2'>
                    <UserNav />
                </div>
                <div className='col'>
                    <h4>Wishlist</h4>
                    {wishlist.map(p => (
                        <div key={p._id} className='alert border border-light'>
                            <Link to={`/product/${p.slug}`}>{p.title}</Link>
                            <span onClick={() => handleRemove(p._id)} className='text-danger float-right pointer' >
                                Remove
                            </span>
                        </div>
                    ))}
                </div>
            </div>
        </div>
    );
};

export default Wishlist;