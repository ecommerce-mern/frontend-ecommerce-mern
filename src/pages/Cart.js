import React from 'react';
import ProductCardInCheckout from '../components/cards/ProductCardInCheckout';
import { Link } from 'react-router-dom';

import { useSelector, useDispatch } from 'react-redux';
import { userCart } from '../functions/user';
import  { toast } from 'react-toastify';

const Cart = ({ history }) => {

    const dispatch = useDispatch();
    const { user, cart } = useSelector((state) => ({ ...state }));
    
    const getTotal = () => {
        return cart.reduce((currentTotal, nextProduct) => {
            return currentTotal + nextProduct.count*nextProduct.price;
        }, 0);
    };

    const showCartItems = () => (
        // const headings = ['Image', 'Title', 'Price', 'Brand', 'Color', 'Count', 'Shipping', 'Remove'];
        <table className='table'>
            <thead className='thead-light'>
                <tr>
                    <th scope='col'>Image</th>
                    <th scope='col'>Title</th>
                    <th scope='col'>Price</th>
                    <th scope='col'>Brand</th>
                    <th scope='col'>Color</th>
                    <th scope='col'>Count</th>
                    <th scope='col'>Shipping</th>
                    <th scope='col'>Remove</th>
                </tr>
            </thead>
            { cart.map((p) => (
                <ProductCardInCheckout key={p._id} p={p} />
            ))}
        </table>
    );

    const saveOrderToDb = () => {
        userCart(cart, user.token)
            .then((res) => {
                if (res.data.ok) {
                    history.push('/checkout');
                };
            })
            .catch((err) => {
                toast.error(err.message);
        });
    };

    return (
        <div className='container-fluid pt-2'>
            <div className='row'>
                <h4 className='pl-4'>Cart</h4>
            </div>
            <div className='row'>
                <div className='col-md-8'>
                    {!cart.length 
                        ? <p>No products in cart</p>
                        : showCartItems()
                    }
                </div>
                <div className='col-md-4'>
                    <h4>Order Summary</h4>
                    <hr />
                    <p>Products</p>
                    {cart.map((p, i) => (
                        <div key={i}>
                            <p>{p.title} x {p.count} = ${p.count * p.price}</p>
                        </div>
                    ))}
                    <hr />
                    Total: <b>${getTotal()}</b>
                    <hr />
                    {
                        user ? (
                            <button onClick={saveOrderToDb} disabled={!cart.length} className='btn btn-primary btn-sm mt-2'>
                                Proceed to checkout
                            </button>
                        ) : (
                            <button className='btn btn-primary btn-sm mt-2'>
                                <Link to={{
                                    pathname: '/login',
                                    state: { from: 'cart' }
                                }}>
                                    Login to checkout
                                </Link>
                            </button>
                        )
                    }
                </div>
            </div>
        </div>
    );
};

export default Cart;