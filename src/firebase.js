import * as firebase from 'firebase';
// import * as initApp from 'firebase/initializeApp';
// import * as authFirebase from 'firebase/auth';
// import * as googleAuth from 'firebase/googleAuthProvider';

// Remove below here, and paste in your own //
const firebaseConfig = {
  apiKey: "AIzaSyCCaJuYhqvL-UZRQT5qsrdap1JkqElWyZA",
  authDomain: "ecommerce-439e2.firebaseapp.com",
  projectId: "ecommerce-439e2",
  storageBucket: "ecommerce-439e2.appspot.com",
  messagingSenderId: "1028779273960",
  appId: "1:1028779273960:web:53113355882536287ce35d"
};

firebase.initializeApp(firebaseConfig);
// Remove down to here //


// export
export const auth = firebase.auth();
export const googleAuthProvider = new firebase.auth.GoogleAuthProvider();
