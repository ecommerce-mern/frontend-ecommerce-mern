import React from 'react';
import StarRatings from 'react-star-ratings';

export const showAverage = (p) => {
    if (p && p.ratings) {
        let ratingsArray = p && p.ratings;
        let total = [];
        let length = ratingsArray.length;

        ratingsArray.map((r) => total.push(r.star));
        let totalReduced = total.reduce((p, n) => p + n, 0);
        let highest = length;
        let result = totalReduced / highest;
    
        return (
            <div className='text-center pt-1 pb-3'>
                <span>
                    <StarRatings 
                        rating={result}
                        starDimension='20px'
                        starSpacing='2px'
                        editing={false}
                        starRatedColor='red'
                    />
                    {' '}
                    ({p.ratings.length})
                </span>
            </div>
        );
    };

};