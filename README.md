# Frontend ecommerce MERN

The Node/React frontend for a fully deployable ecommerce website using Stripe

This frontend is meant to be used with the corresponding backend, which can be found [here](https://gitlab.com/Hetland/ecommerce_server)


# Getting started

### Development mode

#### Step 1
* Clone this repository
* Go into the root folder, and run 'npm i'
* Sign up to Firebase, then create a new application on Firebase and enable Google as a sign-in method
* Register a new web app to your Firebase application, and copy the content inside the script tag into the specified place in src/firebase.js
* Sign up to Stripe
* Then, in the root folder, create a .env file and add the following
~~~
REACT_APP_REGISTER_REDIRECT_URL='http://localhost:3000/register/complete'
REACT_APP_FORGOT_PASSWORD_REDIRECT='http://localhost:3000/login'
REACT_APP_API = 'http://localhost:8000/api'
REACT_APP_STRIPE_KEY=pk_test_XXXXXXXXXXXXXXXXXXXXXXXXXXX
~~~~
where the REACT_APP_STRIPE_KEY is your public Stripe test key
*  ### Now do all the steps for the backend before continuing ###
* Run 'npm start'


# Prerequisites
* Node


# Authors
* Jens Christian Thiemann Hetland

# Images
![Main page](public/front_page.png)
![Admin dashboard](public/admin_dashboard.png)
